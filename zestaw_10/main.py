# encoding: utf-8

import random
import unittest


class Stack:

    def __init__(self, size=10):
        self.items = size * [None]      # utworzenie tablicy
        self.n = 0                      # liczba elementów na stosie
        self.size = size

    def is_empty(self):
        return self.n == 0

    def is_full(self):
        return self.size == self.n

    def push(self, data):
        if self.is_full():
            raise Exception("Pelny stos")
        self.items[self.n] = data
        self.n += 1

    def pop(self):
        if self.is_empty():
            raise Exception("Pusty stos")
        self.n = self.n - 1
        data = self.items[self.n]
        self.items[self.n] = None    # usuwam referencjê
        return data


class Queue:
    def __init__(self, size=5):
        self.n = size + 1         # faktyczny rozmiar tablicy
        self.items = self.n * [None]
        self.head = 0           # pierwszy do pobrania
        self.tail = 0           # pierwsze wolne

    def is_empty(self):
        return self.head == self.tail

    def is_full(self):
        return (self.head + self.n-1) % self.n == self.tail

    def put(self, data):
        if self.is_full():
            raise Exception("pelna lista")
        self.items[self.tail] = data
        self.tail = (self.tail + 1) % self.n

    def get(self):
        if self.is_empty():
            raise Exception("pusta lista")
        data = self.items[self.head]
        self.items[self.head] = None      # usuwam referencjê
        self.head = (self.head + 1) % self.n
        return data


class StackTestCase(unittest.TestCase):
    def test_empty_stack(self):
        stack = Stack(5)
        self.assertRaises(Exception, lambda: stack.pop())

    def test_is_full(self):
        stack = Stack(0)
        self.assertRaises(Exception, lambda: stack.push("data"))

    def test_push(self):
        stack = Stack(1)
        stack.push('test')
        self.assertEqual(stack.items[0], 'test')

    def test_pop(self):
        stack = Stack(2)
        stack.push('test')
        stack.push('test2')
        self.assertEqual(stack.pop(), 'test2')


class TestQueue(unittest.TestCase):
    def test_is_empty(self):
        q = Queue()
        self.assertTrue(q.is_empty())
        q.put(4)
        self.assertFalse(q.is_empty())

    def test_if_full(self):
        q = Queue(2)
        self.assertFalse(q.is_full())
        q.put(4)
        self.assertFalse(q.is_full())
        q.put(8)
        self.assertTrue(q.is_full())

    def test_put(self):
        q = Queue(2)
        q.put(2)
        q.put(4)
        self.assertEqual(q.items, [2, 4, None])
        self.assertRaises(Exception, lambda: q.put(16))

    def test_get(self):
        q = Queue()
        self.assertRaises(Exception, lambda: q.get())


class RandomQueue:
    def __init__(self):
        self.count = 0
        self.items = 200 * [None]

    def insert(self, data):
        if self.is_full():
            raise Exception("Pełna kolejka")
        self.items[self.count] = data
        self.count += 1

    def remove(self):  # zwraca losowy element
        if self.is_empty():
            raise Exception("Pusta kolejka")
        n = random.randint(0, self.count - 1)
        r = self.items[n]
        self.items[n] = self.items[self.count - 1]
        self.items[self.count - 1] = None
        self.count -= 1
        return r

    def is_empty(self):
        return self.count == 0

    def is_full(self):
        return self.count == 200


q = RandomQueue()

for i in range(0, 20):
    q.insert(random.randint(1, 1000))

print q.remove()
print q.remove()
print q.remove()
