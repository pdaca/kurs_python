#!/usr/bin/env python

from random import randint


class Player(object):

    def __init__(self, board, name, is_human, turn):
        self.name = name
        self.board = board
        self.already_shooted = []
        self.is_human = is_human
        self.turn = turn
        self.targets_stack = []
        self.moves = 0

    def validate_shoot(self, enemy, x, y):
        max_x = enemy.board.size - 1
        max_y = enemy.board.size - 1

        if x > max_x or y > max_y:
            return False
        elif x < 0 or y < 0:
            return False

        if (x, y) in self.already_shooted:
            if self.is_human:
                print "You have already shooted in that field!"
            return False

        return True

    def add_targets_to_stack(self, cords, enemy):
        if cords not in self.already_shooted:
            x, y = cords
            if self.validate_shoot(enemy, x, y):
                self.targets_stack.append(cords)

    def make_ai_shoot(self, enemy):
        while self.turn:
            while True:
                try:
                    x, y = self.targets_stack.pop()
                except IndexError:
                    x = randint(0, self.board.size - 1)
                    y = randint(0, self.board.size - 1)

                if self.validate_shoot(enemy, x, y):
                    self.moves += 1
                    if enemy.board.board[x][y] == '+':

                        ship = enemy.board.get_ship_by_cords(x, y)
                        enemy.board.board[x][y] = '-'
                        self.already_shooted.append((x, y))

                        N = (x + 1, y)
                        S = (x - 1, y)
                        E = (x, y + 1)
                        W = (x, y - 1)

                        self.add_targets_to_stack(N, enemy)
                        self.add_targets_to_stack(S, enemy)
                        self.add_targets_to_stack(E, enemy)
                        self.add_targets_to_stack(W, enemy)

                        if not ship.is_destroyed(enemy.board.board):
                            print "{player} has hit your ship!".format(
                                player=self.name
                            )
                        else:
                            print "{player} has destroyed your ship!".format(
                                player=self.name
                            )
                            enemy.board.ships.remove(ship)

                        break

                    else:
                        self.turn = False
                        enemy.turn = True
                        enemy.board.board[x][y] = 'o'
                        print "{player} has missed!".format(
                            player=self.name
                        )   
                        break
                else:
                    x = randint(0, self.board.size - 1)
                    y = randint(0, self.board.size - 1)
                    continue

    def make_shoot(self, enemy, x, y):
        self.moves += 1
        if self.validate_shoot(enemy, x, y):
            if enemy.board.board[x][y] == '+':
                self.turn = True
                ship = enemy.board.get_ship_by_cords(x, y)
                self.board.draft_board[x][y] = '+'
                enemy.board.board[x][y] = '-'
                if ship.is_destroyed(enemy.board.board):
                    enemy.board.ships.remove(ship)
                    print "{player} has destroyed ship!".format(
                        player=self.name
                    )
                else:
                    print "{player} has hit a ship!".format(
                        player=self.name
                    )
            else:
                enemy.turn = True
                self.turn = False
                print "{player} has missed!".format(
                    player=self.name
                )
                enemy.board.board[x][y] = 'o'
                self.board.draft_board[x][y] = 'o'
            self.already_shooted.append((x, y))
            return True
        return False
