# 4.2
def build_measure(size):
    unit = '|....|'
    measure = [unit[1:] if m != 0 else unit for m in range(size - 1)]
    description = [str(n + 1).rjust(5) if n != 0 else str(n + 1) for n in range(size)]

    return ''.join(measure) + '\n' + ''.join(description)


def build_rectangle():
    string = ''
    horizontal = '+---+---+---+---+'
    vertical = '|   |   |   |   |'

    string += 7 * (horizontal + '\n' + vertical + '\n') + horizontal

    return string


# 4.3
def factorial(n):
    result = 1
    while n >= 1:
        result = result * n
        n = n - 1
    return result


# 4.4
def fibonacci(n):
    f0, f1 = 0, 1
    for n in range(n):
        f0, f1 = f1, f0 + f1

    return f0


# 4.5
def odwracanie(L, left, right):
    for i, x in enumerate(L[left:right + 1]):
        L[right - i] = x

    return L


def odwracanie_rek(L, left, right):
  if left != right and left < right:
    tmp = L[left]
    L[left] = L[right]
    L[right] = tmp
    odwracanie_rek(L, left+1, right-1)


# 4.6
def sum_seq(sequence):
    total_sum = 0
    for i in sequence:
        if isinstance(i, (list, tuple)):
            total_sum += sum_seq(i)
        else:
            total_sum += i
    return total_sum


# 4.7
seq = [1, (2, 3), [], [4, (5, 6, 7)], 8, [9]]


def flatten(sequence):
    flat = []
    for i, item in enumerate(sequence):
        if isinstance(item, (list, tuple)):
            flat.extend(flatten(item))
        else:
            flat.append(item)
    return flat

