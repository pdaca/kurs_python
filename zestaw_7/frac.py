# encoding: utf-8

import unittest

from fractions import gcd


class Frac:
    """Klasa reprezentująca ułamki."""

    def shorten(self):
        n = gcd(self.x, self.y)
        self.x /= n
        self.y /= n

        return self

    def __init__(self, x=0, y=1):
        if y == 0:
            raise ValueError
        self.x = x
        self.y = y
        self.shorten()

    def __str__(self):
        if self.y == 1:
            return str(self.x)

        return "{}/{}".format(self.x, self.y)

    def __repr__(self):
        return "Frac({}, {})".format(self.x, self.y)

    def __cmp__(self, other):
        self.shorten()
        other.shorten()
        if self.x == other.x and self.y == other.y:
            return 0
        else:
            self.x = self.x * other.y
            other.x = other.x * self.y
            if self.x > other.x:
                return 1

            return -1

    def __add__(self, other):
        if isinstance(other, (int, long, float)):
            return Frac(self.x + self.y * other, self.y).shorten()
        else:
            licznik = self.x * other.y + other.x * self.y
            mianownik = self.y * other.y

            return Frac(licznik, mianownik).shorten()

    __radd__ = __add__              # int+frac

    def __sub__(self, other):
        if isinstance(other, (int, long, float)):
            return Frac(self.x - self.y * other, self.y).shorten()
        else:
            licznik = self.x * other.y - other.x * self.y
            mianownik = self.y * other.y
            return Frac(licznik, mianownik).shorten()

    def __rsub__(self, other):      # int-frac
        # tutaj self jest frac, a other jest int!
        if isinstance(other, (int, long, float)):
            return Frac(self.y * other - self.x, self.y).shorten()

    def __mul__(self, other):
        if isinstance(other, (int, long, float)):
            return Frac(self.x * other,self.y).shorten()
        else:
            return Frac(self.x * other.x, self.y * other.y).shorten()

    __rmul__ = __mul__              # int*frac

    def __div__(self, other):
        if isinstance(other, (int, long, float)):
            if other == 0:
                raise ValueError
            self.y = self.y * other
            return self.shorten()
        else:
            if other.x == 0:
                raise ValueError
            self.x = self.x * other.y
            self.y = self.y * other.x
            return self.shorten()

    def __rdiv__(self, other):
        if isinstance(other, (int, long, float)):
            if other == 0:
                return Frac(0, 1)
            if self.x == 0:
                raise ValueError
            return Frac(other * self.y, self.x).shorten()

    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        return self

    def __neg__(self):
        self.x = -self.x
        return self

    def __invert__(self):
        if self.x == 0:
            raise ValueError
        else:
            t = self.x
            self.x = self.y
            self.y = t
            return self


class TestFrac(unittest.TestCase):

    def setUp(self):
        self.x = 0
        self.y = 1

    def test__str__(self):
        self.assertEqual(Frac(1, 2).__str__(), "1/2")
        self.assertEqual(Frac(4, 1).__str__(), "4")

    def test__repr__(self):
        self.assertEqual(Frac(1, 2).__repr__(), "Frac(1, 2)")
        self.assertEqual(Frac(4, 1).__repr__(), "Frac(4, 1)")

    def test__cmp__(self):
        self.assertTrue(Frac(1, 2) == Frac(4, 8))
        self.assertFalse(Frac(4, 7) == Frac(4, 6))
        self.assertTrue(Frac(1, 2) <= Frac(1, 2))
        self.assertTrue(Frac(3, 4) <= Frac(4, 5))
        self.assertFalse(Frac(1, 2) <= Frac(1, 4))
        self.assertTrue(Frac(3, 4) <= Frac(4, 5))
        self.assertTrue(Frac(3, 4) < Frac(7, 5))
        self.assertFalse(Frac(4, 5) < Frac(1, 8))
        self.assertTrue(Frac(7, 15) >= Frac(1, 5))
        self.assertFalse(Frac(7, 15) >= Frac(8, 5))
        self.assertTrue(Frac(4, 3) < Frac(9, 5))
        self.assertTrue(Frac(4, 3) < Frac(18, 5))

    def test__add__(self):
        self.assertEqual(Frac(1, 2) + Frac(1, 2), Frac(1, 1))
        self.assertEqual(Frac(1, 2) + Frac(5, 8), Frac(9, 8))
        self.assertEqual(Frac(4, 7) + 1, Frac(11, 7))

    def test__radd__(self):
        self.assertEqual(1 + Frac(4, 7), Frac(11, 7))

    def test__sub__(self):
        self.assertEqual(Frac(3, 4) - Frac(1,2), Frac(1, 4))
        self.assertEqual(Frac(8, 3) - 4, Frac(-4, 3))

    def test__rsub__(self):
        self.assertEqual(4 - Frac(8, 3), Frac(4, 3))

    def test__mul__(self):
        self.assertEqual(Frac(1, 2) * Frac(1, 2), Frac(1, 4))
        self.assertEqual(Frac(1, 2) * 4, Frac(2, 1))

    def test__rmul__(self):
        self.assertEqual(4 * Frac(1, 2), Frac(2, 1))

    def test__div__(self):
        self.assertEqual(Frac(1, 2) / 4, Frac(1, 8))
        self.assertEqual((Frac(1, 3)) / Frac(5, 17), Frac(17, 15))

    def test__rdiv__(self):
        self.assertEqual(8 / Frac(4, 5), Frac(10, 1))

    def test__pos__(self):
        self.assertEqual(+Frac(1, 2), Frac(1, 2))

    def test__neg__(self):
        self.assertEqual(-Frac(4, 5), Frac(-4, 5))
        self.assertEqual(-Frac(-4, 5), Frac(4, 5))
        self.assertEqual(-Frac(4, -5), Frac(4, 5))
        self.assertEqual(-Frac(-4, -5), Frac(-4, 5))

    def test__invert__(self):
        self.assertEqual(~Frac(1, 2), Frac(2, 1))
        self.assertEqual(~Frac(8, 1), Frac(1, 8))
