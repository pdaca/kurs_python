import unittest

from point import Point


class TestPoint(unittest.TestCase):
    def setUp(self):
        pass

    def test_print(self):
        self.assertEqual(str(Point(1, 2)), "(1, 2)")
        self.assertEqual(repr(Point(2, 3)), "Point(2, 3)")

    def test_eq(self):
        self.assertTrue(Point(1, 2) == Point(1, 2))
        self.assertTrue(Point(1, 2) != Point(1, 1))

    def test_add(self):
        self.assertEqual(Point(1, 1) + Point(1, 3), (2, 4))

    def test_sub(self):
        self.assertEqual(Point(2, 4) - Point(0, 2), (2, 2))

    def test_multiply(self):
        self.assertEqual(Point(2, 3) * Point(4, 5), 23)

    def test_cross(self):
        self.assertEqual(Point(4, 3).cross(Point(1, 2)), 5)

    def test_length(self):
        self.assertEqual(Point(3, 4).length(), 5)


if __name__ == '__main__':
    unittest.main()
