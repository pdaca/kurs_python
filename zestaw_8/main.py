# encoding: utf-8

from __future__ import division

import random
import math


def solve1(a, b, c):
    """Rozwiązywanie równania liniowego a x + b y + c = 0."""
    if a == b == 0:
        print "Tożsamość"
    elif a == 0 and b != 0:
        print str(-c / b)
    elif a != 0 and b == 0:
        print str(-c / a)
    elif c == 0:
        print str(-a / b)
    else:
        print str(a / b), str(-c / b)


class Point:
    def __init__(self):
        self.x = random.random()
        self.y = random.random()


def calc_pi(n=100):
    """Obliczanie liczby pi metoda Monte Carlo.
    n oznacza liczbe losowanych punktow."""
    l_k = 0
    for i in range(0, n):
        point = Point()
        if math.sqrt(point.x * point.x + point.y * point.y) <= 1:
            l_k += 1
    return 4 * l_k / n


def heron(a, b, c):
    """Obliczanie pola powierzchni trojkata za pomoca wzoru
    Herona. Dlugosci bokow trojkata wynosza a, b, c."""

    if a + b <= c or b + c <= a or a + c <= b:
        raise ValueError
    p = 0.5 * (a + b + c)

    return math.sqrt(p * (p - a) * (p - b) * (p - c))


solve1(0, 0, 0)
solve1(0, 2, 4)
solve1(2, 0, 2)
solve1(1, 0, 0)
solve1(1, -2, 2)

print calc_pi(10000000)

print "Pole trójkąta: ", heron(3, 4, 5)


def p_rek(i=0, j=0):
    if i < 0 or j < 0:
        raise ValueError
    if i == 0 and j == 0:
        return 0.5
    elif j == 0:
        return 0
    elif i == 0:
        return 1
    else:
        return 0.5 * (p_rek(i - 1, j) + p_rek(i, j - 1))

print "Rezultat funkcji P (rek)", p_rek(4, 8)


def P_dyn(i=0, j=0):
    if i == 0 and j == 0:
        return 0.5
    elif j == 0:
        return 0
    elif i == 0:
        return 1
    else:
        dict = {(0, 0): 0.5, (0, 1): 1}
        for a in range(1, j + 1):
            dict[(0, a)] = 1
        for b in range(1, i + 1):
            dict[(b, 0)] = 0
        for n in range(1, i + 1):
            for m in range(1, j + 1):
                dict[(n, m)] = 0.5 * (dict[(n - 1, m)] + dict[(n, m - 1)])
        print dict[(n, m)]

print "Rezultat funkcji P (dyn)", P_dyn(4, 8)
