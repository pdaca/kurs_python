# encoding: utf-8

import random


def swap(L, left, right):
    """Zamiana miejscami dwóch elementów."""
    # L[left], L[right] = L[right], L[left]
    item = L[left]
    L[left] = L[right]
    L[right] = item


def bubblesort(L, left, right):
    for i in range(left, right):
        for j in range(left, right):
            if L[j] > L[j + 1]:
                swap(L, j + 1, j)
    return L


def fullRandom(size=10, k=5):  # w pełni losowe
    L = size * [None]
    for i in range(0, size):
        L[i] = random.randint(0, k - 1)
    return L


def mediana_sort(L, left, right):
    if left >= len(L) or right >= len(L):
        return

    if left < 0 or right < 0:
        return

    if left > right:
        t = left
        left = right
        right = t

    length = right - left + 1

    L2 = L[left : right + 1]
    L2 = bubblesort(L2, 0, length - 1)
    print L2

    if length % 2 == 0:
        return (L2[length / 2 - 1] + L2[length / 2]) * 0.5
    else:
        return L2[length / 2]


def moda_sort(L, left, right):
    L = bubblesort(L, left, right)
    """Wyszukiwanie mody w ciągu."""
    if left + 1 > right:
        return
    i1 = None  # najlepszy kandydat (indeks)
    number1 = 0  # jego liczebność
    i2 = left  # bieżący element (indeks)
    number2 = 1  # jego liczebność
    while i2 < right:
        i2 = i2 + 1
        if L[i2] == L[i2 - 1]:  # je¿eli się powtórzył
            number2 = number2 + 1
            # na bieżąco uaktualniamy najlepszego kandydata
            if number2 > number1:  # jest lepszy kandydat
                number1 = number2
                i1 = i2
            elif number2 == number1:
                i1 = -1
        else:  # nowy bieżący element
            number2 = 1
    if i1 == -1:
        i1 = None
    return i1


L = fullRandom(25, 10)
i = mediana_sort(L, 5, 0)
print "mediana =", i
i = mediana_sort(L, 0, 5)
print "mediana =", i
i = mediana_sort(L, 5, 15)
print "mediana =", i

L = fullRandom(25, 10)
i = moda_sort(L, 0, len(L) - 1)
print L
if i is None:
    print "w tym zbiorze nie ma mody"
else:
    print "i =", i
    print "L[%i] = %d" % (i, L[i])
