L = [2, 3, 5, 1, 7, 4]

# 2.10
line = "Lorem ipsum dolor\nsit amet turpis\negestas\t tristique " \
       "senectus et."
print len(line.split())

# 2.11
word = "dummyWord"
print '_'.join([c for c in word])

# 2.12
rows = line.split('\n')
print rows[0], rows[-1]

# 2.13
words = line.split()
print sum(map(len, line.split()))

# 2.14
print max(line.split(), key=len)

# 2.15
print ''.join(map(str, L))

# 2.16
print line.replace("GvR", "Guido van Rossum")

# 2.17
print sorted(line.split(), key=str.lower)
print sorted(line.split(), key=len)

# 2.18
digit = 2039002300
print len([d for d in str(digit) if d == '0'])

# 2.19
L = [3, 5, 23, 83, 94, 928, 372, 652]
print map(lambda x: str(x).zfill(3) if len(str(x)) < 3 else str(x), L)
