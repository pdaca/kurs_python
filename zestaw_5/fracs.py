# encoding: utf-8

import unittest

from fractions import gcd


def simplify_fraction(numer, denom):
    common_divisor = gcd(numer, denom)
    return [numer / common_divisor, denom / common_divisor]


def add_frac(frac1, frac2):
    # frac1 + frac2
    nominator = (frac1[0] * frac2[1]) + (frac2[0] * frac1[1])
    denominator = frac1[1] * frac2[1]

    return simplify_fraction(nominator, denominator)


def sub_frac(frac1, frac2):
    # frac1 - frac2
    nominator = (frac1[0] * frac2[1]) - (frac2[0] * frac1[1])
    denominator = frac1[1] * frac2[1]

    return simplify_fraction(nominator, denominator)


def mul_frac(frac1, frac2):
    # frac1 * frac2
    nominator = frac1[0] * frac2[0]
    denominator = frac1[1] * frac2[1]

    return simplify_fraction(nominator, denominator)


def div_frac(frac1, frac2):
    # frac1 / frac2
    nominator = frac1[0] * frac2[1]
    denominator = frac1[1] * frac2[0]

    return simplify_fraction(nominator, denominator)


def is_positive(frac):
    # bool, czy dodatni
    return (float(frac[0]) / float(frac[1])) > 0


def is_zero(frac):
    # bool, typu [0, x]
    return 0 in frac


def cmp_frac(frac1, frac2):
    float_frac1 = float(frac1[0]) / float(frac1[1])
    float_frac2 = float(frac2[0]) / float(frac2[1])

    if float_frac1 > float_frac2:
        return 1
    elif float_frac1 == float_frac2:
        return 0
    elif float_frac1 < float_frac2:
        return -1


def frac2float(frac):
    return float(frac[0]) / float(frac[1])


class TestFractions(unittest.TestCase):

    def setUp(self):
        self.zero = [0, 1]

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])

    def test_sub_frac(self):
        self.assertEqual(sub_frac([3, 4], [1, 3]), [5, 12])

    def test_mul_frac(self):
        self.assertEqual(mul_frac([1, 3], [1, 6]), [1, 18])

    def test_div_frac(self):
        self.assertEqual(div_frac([1, 2], [1, 4]), [2, 1])

    def test_is_positive(self):
        self.assertFalse(is_positive([-1, 2]))
        self.assertTrue(is_positive([1, 2]))

    def test_is_zero(self):
        self.assertTrue(is_zero([0, 3]))
        self.assertFalse(is_zero([1, 3]))

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([1, 2], [1, 2]), 0)
        self.assertEqual(cmp_frac([1, 2], [1, 3]), 1)
        self.assertEqual(cmp_frac([1, 4], [1, 2]), -1)

    def test_frac2float(self):
        self.assertEqual(frac2float([1, 2]), 0.5)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()
