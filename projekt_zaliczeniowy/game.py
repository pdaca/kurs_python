﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from datetime import datetime

from battleship.board import Board
from battleship.player import Player

if len(sys.argv) > 1:
    if sys.argv[1] == '-h':
        print "\n---- Gra w statki ----\n\nKażdy z graczy posiada po dwie plansze o wielkości od 5x5 do 10x10.\n" \
              "Na jednym z kwadratów gracz zaznacza swoje statki, których położenie będzie odgadywał przeciwnik.\n" \
              "Nieodkryte pole oznaczone jest symbolem '-', niecelny strzał 'o', natomiast okręt to '+'.\n" \
              "Wielkość okrętów oraz ich położenie jest losowane.\n\n"
        exit(0)


class Config(object):

    BOARD_SIZE = 0
    SHIPS = 0
    AI_START_FIRST = 0
    ALLOW_STATS = 0

    ERROR_MESSAGE = "Configuration file is broken!"

    @classmethod
    def read_config_file(cls):
        options = dict()
        with open('game.cfg') as f:
            try:
                for line in f:
                    line = line.splitlines()
                    for y in line:
                        option = y.split("=")
                        options[option[0]] = option[1]
            except:
                print cls.ERROR_MESSAGE
                exit(1)

        return options

    @classmethod
    def apply_config(cls):
        try:
            config = cls.read_config_file()
            cls.BOARD_SIZE = int(config['BOARD_SIZE'])
            cls.SHIPS = int(config['SHIPS'])
            cls.AI_START_FIRST = int(config['AI_START_FIRST'])
            cls.ALLOW_STATS = int(config['ALLOW_STATS'])
        except:
            print cls.ERROR_MESSAGE
            exit(1)

Config.apply_config()


class Game(object):
    def __init__(self):
        self.is_over = False
        self.letters = dict(
            A=1,
            B=2,
            C=3,
            D=4,
            E=5,
            F=6,
            G=7,
            H=8,
            I=9,
        )

    def save_game_stats(self, player_name, date, moves, ships_left, wl):
        f = open('scores.txt', 'a')
        line = "{player_name};{date};{moves};{ships_left};{wl}\n".format(
            player_name=player_name,
            date=date,
            moves=moves,
            ships_left=ships_left,
            wl=wl
        )
        f.write(line)
        f.close()

    def get_winner(self, player_one, player_two):
        if player_one.board.ships:
            return player_one
        elif player_two.board.ships:
            return player_two
        else:
            return False

    def check_over(self, player_one, player_two):
        if not player_one.board.ships:
            print "{player} has won in {moves} moves!".format(
                player=player_two.name,
                moves=player_two.moves
            )

            return True
        elif not player_two.board.ships:
            print "{player} has won in {moves} moves!".format(
                player=player_one.name,
                moves=player_one.moves
            )

            return True

        return False

    def validate_field(self, field):
        if len(field) != 2:
            return False
        try:
            self.letters[field[0]]
            int(field[1])
        except Exception:
            return False

        return True

    def parse_field(self, field):

        letter = field[0]
        y = int(self.letters[letter]) - 1
        x = int(field[1])

        return x, y

    def start(self):
        valid_nickname = False
        while not valid_nickname:
            player_name = raw_input("Please enter your nickname: \n")
            if len(player_name) > 1:
                valid_nickname = True
            else:
                print "Your nickname is too short."

        if Config.AI_START_FIRST:
            player = Player(Board(Config.BOARD_SIZE), player_name, False, False)
            player_ai = Player(Board(Config.BOARD_SIZE), "Computer", False, True)
        else:
            player = Player(Board(Config.BOARD_SIZE), player_name, False, True)
            player_ai = Player(Board(Config.BOARD_SIZE), "Computer", False, False)

        for x in range(Config.SHIPS):
            player.board.randomly_place_ship()
            player_ai.board.randomly_place_ship()

        while True:
            print
            print "Your board:"
            player.board.print_board()
            print
            print "Enemy board:"
            player.board.print_board(True)

            if player.turn:
                while True:
                    field = raw_input("Where do you want to shoot? (Example: B7): \n")
                    if self.validate_field(field):
                        x, y = self.parse_field(field)
                        player.make_shoot(player_ai, x, y)
                        break
                    else:
                        print "Incorrect field format, try again."
                        continue
            else:
                player_ai.make_ai_shoot(player)

            if self.check_over(player, player_ai):
                print "Game Over!"
                print "Your board:"
                player.board.print_board()
                print
                print "Enemy board:"
                player.board.print_board(True)
                if Config.ALLOW_STATS:
                    winner = self.get_winner(player, player_ai)
                    if winner == player:
                        self.save_game_stats(
                            player.name,
                            datetime.today().strftime("%d-%m-%Y, %H:%M"),
                            player.moves,
                            len(player.board.ships),
                            'W'
                        )
                    else:
                        self.save_game_stats(
                            player.name,
                            datetime.today().strftime("%d-%m-%Y, %H:%M"),
                            player.moves,
                            len(player.board.ships), 'L'
                        )
                break
            else:
                continue

game = Game()
game.start()
