# encoding: utf-8


class Node:
    """Klasa reprezentuj±ca wêze³ listy jednokierunkowej."""

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)


def find_max(head):
    if not head:
        return
    max_value = head.data
    while head:
        if max_value < head.data:
            max_value = head.data
        head = head.next
    return max_value


def find_min(head):
    if not head:
        return
    min_value = head.data
    while head:
        if min_value > head.data:
            min_value = head.data
        head = head.next
    return min_value


head = None

head = Node(9, head)
head = Node(4, head)
head = Node(1, head)
head = Node(-1, head)
head = Node(8, head)
head = Node(15, head)
head = Node(2, head)

print "Max: ", find_max(head)
print "Min: ", find_min(head)


class TreeNode:
    """Klasa reprezentująca węzły drzewa binarnego."""

    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)


def count_leafs(top):
    count = 0

    if not top:
        return count
    if not top.left and not top.right:
        count += 1

    count += count_leafs(top.left)
    count += count_leafs(top.right)

    return count


def count_total(top):
    count = 0
    if not top or not top.data:
        return count

    count += top.data
    count += count_total(top.left)
    count += count_total(top.right)

    return count


root = TreeNode(1)
root.left = TreeNode(2)
root.right = TreeNode(3)
root.left.left = TreeNode(6)
root.left.right = TreeNode(2)
root.right.left = TreeNode(9)
root.right.right = TreeNode(1)

print "Ilość liści: ", count_leafs(root)
print "Suma liczb: ", count_total(root)
