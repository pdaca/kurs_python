#!/usr/bin/env python

from ship import Ship
from random import randint
from string import ascii_uppercase


class Board(object):
    def __init__(self, size):
        self.size = size
        self.board = self.generate_board()
        self.ships = []
        self.draft_board = self.generate_board()

    def generate_board(self):
        return [['-' for x in range(self.size)] for y in range(self.size)]

    def validate(self, x, y, ship):
        max_index = self.size - 1

        if ship.orientation == 'v':
            if x + ship.size > max_index or x < 0:
                return False
            else:
                for s in range(ship.size):
                    try:
                        if self.board[x + s][y] == '+':
                            return False
                    except IndexError:
                        return False
                return True

        elif ship.orientation == 'h':
            if y + ship.size > max_index or y < 0:
                return False
            else:
                for s in range(ship.size):
                    try:
                        if self.board[x][y + s] == '+':
                            return False
                    except IndexError:
                        return False
                return True

    def place_ship(self, ship, x, y):
        if self.validate(x, y, ship):
            if ship.orientation == 'h':
                for i in range(ship.size):
                    self.board[x][y + i] = '+'
                self.ships.append(ship)
                ship.calculate_cords((x, y))
            elif ship.orientation == 'v':
                for i in range(ship.size):
                    self.board[x + i][y] = '+'

                self.ships.append(ship)
                ship.calculate_cords((x, y))
            else:
                print "Couldn't get ship orientation."
        else:
            return False

    def randomly_place_ship(self, ship_size=None):
        orientations = ('h', 'v')

        valid = False
        while not valid:
            random_orientation = orientations[randint(0, 1)]
            random_x = randint(0, 7)
            random_y = randint(0, 7)

            if not ship_size:
                ship_size = randint(1, 4)
            ship = Ship(ship_size, random_orientation)

            valid = self.validate(random_x, random_y, ship)
            if valid:
                self.place_ship(ship, random_x, random_y)

    def print_board(self, draft=False):
        if not draft:
            board = self.board
        else:
            board = self.draft_board

        print '--',
        symbols_length = self.size 
        for i, x in enumerate(ascii_uppercase[:self.size]):
            if i == symbols_length - 1:
                print ' '.join(x)
            else:
                print ' '.join(x),
        for i, x in enumerate(board):
            print str(i) + ' ',
            print ' '.join(x)

    def get_ship_by_cords(self, x, y):
        for ship in self.ships:
            if (x, y) in ship.cords:
                return ship
