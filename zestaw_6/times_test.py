# encoding: utf-8

import unittest

from times import Time


class TestTime(unittest.TestCase):

    def setUp(self): pass

    def test_print(self):
        self.assertEqual(str(Time(3600)), "01:00:00")
        self.assertEqual(repr(Time(1800)), "Time(1800)")

    def test_add(self):
        self.assertEqual(Time(1) + Time(2), Time(3))

    def test_cmp(self):
        self.assertTrue(Time(1) == Time(1))
        self.assertTrue(Time(1) != Time(2))
        self.assertTrue(Time(3) > Time(2))

    def test_int(self):
        self.assertEqual(int(Time(10)), 10)

    def tearDown(self): pass


if __name__ == "__main__":
    unittest.main()
