# 4.3
def factorial(n):
    result = 1
    while n >= 1:
        result = result * n
        n = n - 1
    return result


# 4.4
def fibonacci(n):
    f0, f1 = 0, 1
    for n in range(n):
        f0, f1 = f1, f0 + f1

    return f0
