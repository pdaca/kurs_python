#!/usr/bin/env python


class Ship(object):
    def __init__(self, size, orientation):
        self.size = size
        self.orientation = orientation
        self.cords = []

    def calculate_cords(self, cords):
        if self.orientation == 'v':
            for i in range(self.size):
                self.cords.append((cords[0] + i, cords[1]))
        elif self.orientation == 'h':
            for i in range(self.size):
                self.cords.append((cords[0], cords[1] + i))

    def is_destroyed(self, board):
        health = self.size
        if self.orientation == 'v':
            for i in range(self.size):
                if board[self.cords[0][0] + i][self.cords[0][1]] == '-':
                    health -= 1
        elif self.orientation == 'h':
            for i in range(self.size):
                if board[self.cords[0][0]][self.cords[0][1] + i] == '-':
                    health -= 1

        if not health:
            return True

        return False
