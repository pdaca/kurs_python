# encoding: utf-8

import random


def swap2(L, left, right):
    """Zamiana miejscami dwóch elementów."""
    # L[left], L[right] = L[right], L[left]
    item = L[left]
    L[left] = L[right]
    L[right] = item


class Data:
    def __init__(self, size=10):
        self.size = size
        self.items = self.size * [None]
        for i in range(0, self.size):
            self.items[i] = i

    def swap(self, i, j):
        temp = self.items[i]
        self.items[i] = self.items[j]
        self.items[j] = temp

    def fullRandom(self):           #w pełni losowe
        # print self.items
        for i in range(0,self.size*self.size):
            j = random.randint(0,self.size-1)
            k = random.randint(0,self.size-1)
            # print i, j
            self.swap(j,k)
        return self

    def closeRandom(self):          #prawie posortowane
        # print self.items
        for i in range(0,self.size):
            # items2 = self.size * [None]
            j = random.randint(0,self.size-1)
            down = max(j-self.size/5, 0)
            up = min(self.size-1, j+self.size/10)
            k = random.randint(down, up)
            self.swap(j,k)
        return self.items

    def closeDivertedRandom(self):  #prawie posortowane w odwrotnej kolejności
        # print self.items
        for i in range(0,self.size):
            # items2 = self.size * [None]
            j = random.randint(0,self.size-1)
            down = max(j-self.size/5, 0)
            up = min(self.size-1, j+self.size/10)
            k = random.randint(down, up)
            self.swap(j,k)

        print self.items
        i = 0
        j = self.size - 1

        while i < j:
            self.swap(i, j)
            i += 1
            j -= 1
        return self

    def selectsort(self):
        for i in range(0, self.size-1):
            k = i
            for j in range(i+1, self.size):
                if self.items[j] < self.items[k]:
                    k = j
            self.swap(i, k)


d = Data(10)
d.closeDivertedRandom().selectsort()

print d.items
