# encoding: utf-8

import unittest

from math import pi

from .point import Point


class Circle:
    """Klasa reprezentująca okręgi na płaszczyźnie."""

    def __init__(self, x=0, y=0, radius=1):
        if radius < 0:
            raise ValueError("promień ujemny")
        self.pt = Point(x, y)
        self.radius = radius

    def __repr__(self):
        return "Circle({x}, {y}, {radius})".format(x=self.pt.x, y=self.pt.y,
                                                   radius=self.radius)

    def __eq__(self, other):
        if isinstance(other, Circle):
            return self.pt == other.pt and self.radius == other.radius

        raise ValueError("Podany obiekt nie jest instancją klasy Circle")

    def __ne__(self, other):
        if isinstance(other, Circle):
            return not self == other

        raise ValueError("Podany obiekt nie jest instancją klasy Circle")

    def area(self):
        return pi * self.radius ** 2

    def move(self, x, y):
        try:
            new_x = float(self.pt.x + x)
            new_y = float(self.pt.y + y)
            self.pt = Point(new_x, new_y)
        except ValueError as e:
            raise Exception("Podane punkty nie są cyframi.")

    def cover(self, other):
        if isinstance(other, Circle):
            R = max(
                (self.radius +
                 other.radius + (other.pt - self.pt).length()) / 2,
                self.radius, other.radius
            )

            temp = (R - self.radius) / (2 * R - self.radius-other.radius)

            y = self.pt.y + temp * (other.pt.y - self.pt.y)
            x = self.pt.x + temp * (other.pt.x - self.pt.y)

            return Circle(x, y, R)

        raise ValueError("Podany obiekt nie jest instancją klasy Circle")


class TestCircle(unittest.TestCase):
    def test_wrong_radius(self):
        self.assertRaises(ValueError, lambda: Circle(1, 2, -1))

    def test_repr(self):
        self.assertEqual(Circle(1, 2, 3).__repr__(), "Circle(1, 2, 3)")

    def test_compare(self):
        correct_circle = Circle(1, 2, 1)
        incorrect_circle = "dummy_string"
        same_circle = Circle(1, 2, 1)
        another_circle = Circle(1, 2, 5)

        self.assertTrue(correct_circle == same_circle)
        self.assertFalse(correct_circle == another_circle)
        self.assertRaises(ValueError,
                          lambda: correct_circle == incorrect_circle)

    def test_area(self):
        self.assertEqual(round(Circle(1, 2, 5).area(), 1), 78.5)

    def test_move(self):
        circle = Circle(1, 2, 3)
        circle.move(2, 3)

        self.assertEqual(circle.pt.x, 3)
        self.assertEqual(circle.pt.y, 5)

    def test_cover(self):
        self.assertEqual(Circle(1, 1, 1).cover(Circle(3, 1, 1)),
                         Circle(2, 1, 2))
        self.assertEqual(Circle(0, 1, 2).cover(Circle(2, 4, 8)),
                         Circle(1, 4, 8))
