# encoding: utf-8

import random


def mergesort(L, left, right):
    """Sortowanie przez scalanie."""
    if left < right:
        middle = (left + right) / 2  # wyznaczanie środka
        mergesort(L, left, middle)
        mergesort(L, middle + 1, right)
        merge(L, left, middle, right)  # scalanie


def merge(L, left, middle, right):
    """£±czenie posortowanych sekwencji."""
    T = [None] * (right - left + 1)
    left1 = left
    right1 = middle
    left2 = middle + 1
    right2 = right
    i = 0
    while left1 <= right1 and left2 <= right2:
        if L[left1] <= L[left2]:  # mniejsze lub równe
            T[i] = L[left1]
            left1 += 1
        else:
            T[i] = L[left2]
            left2 += 1
        i += 1
    # Lewa lub prawa część może mieć elementy.
    while left1 <= right1:
        T[i] = L[left1]
        left1 += 1
        i += 1
    while left2 <= right2:
        T[i] = L[left2]
        left2 += 1
        i += 1
    # Skopiuj z tablicy tymczasowej do oryginalnej.
    for i in range(right - left + 1):
        L[left + i] = T[i]


class Data:
    def __init__(self, size=10):
        self.size = size
        self.items = self.size * [None]
        for i in range(0, self.size):
            self.items[i] = i

    def swap(self, i, j):
        temp = self.items[i]
        self.items[i] = self.items[j]
        self.items[j] = temp

    def fullRandom(self):  # w pełni losowe
        for i in range(0, self.size * 2):
            j = random.randint(0, self.size - 1)
            k = random.randint(0, self.size - 1)
            self.swap(j, k)
        return self

    def closeRandom(self):  # prawie posortowane
        for i in range(0, self.size * 2):
            j = random.randint(0, self.size - 1)
            down = max(j - self.size / 5, 0)
            up = min(self.size - 1, j + self.size / 10)
            k = random.randint(down, up)
            self.swap(j, k)
        return self

    def closeDivertedRandom(self):  # prawie posortowane w odwrotnej kolejności
        for i in range(0, self.size * 2):
            j = random.randint(0, self.size - 1)
            down = max(j - self.size / 5, 0)
            up = min(self.size - 1, j + self.size / 10)
            k = random.randint(down, up)
            self.swap(j, k)

        i = 0
        j = self.size - 1

        while i < j:
            self.swap(i, j)
            i += 1
            j -= 1
        return self

    def selectsort(self):
        for i in range(0, self.size - 1):
            k = i
            for j in range(i + 1, self.size):
                if self.items[j] < self.items[k]:
                    k = j
            self.swap(i, k)

    def bubblesort(self):
        limit = self.size - 1
        while True:
            k = -1  # wskaźnik przestawianej pary
            for i in range(0, limit):
                if self.items[i] > self.items[i + 1]:
                    self.swap(i, i + 1)
                    k = i
            if k > 0:
                limit = k
            else:
                break
