# encoding: UTF-8

# 3.1

x = 2 ; y = 3 ;
if (x > y):
    result = x;
else:
    result = y;

print result;

# TAK

# for i in "qwerty": if ord(i) < 100: print i

# NIE

for i in "axby": print ord(i) if ord(i) < 100 else i

# TAK

# 3.2

L = [3, 5, 4] ; L = L.sort()

# Podczas przypisania tworzy się nowa, pusta lista w L.

# x, y = 1, 2, 3

# Brakuje jednej zmiennej, tak aby przypisać liczbę 3

# X = 1, 2, 3 ; X[1] = 4

# Nie można modyfikować zawartości tupli

# X = [1, 2, 3] ; X[3] = 4

# Nie ma takiego indeksu jak 3 w liście X. Aby dodać nowy element
# należy skorzystać z funkcji append()

# X = "abc" ; X.append("d")

# String nie jest listą

# map(pow, range(8))

# Funkcja pow oczekuje dwóch argumentów. Rozwiązaniem może być funkcja lambda 
# map(lambda x: pow(x, 2), range(8))

# 3.3

for x in range(31):
    if x % 3 != 0:
        print x,

# 3.4

while(True):
    user_input = raw_input("\nPodaj liczbę rzeczywistą: ")
    if user_input == 'stop':
        break

    try:
        user_input = float(user_input)
        print user_input, pow(user_input, 3)
    except ValueError:
        print "Niepoprawne użycie programu: nie podano liczby."
        continue


# 3.5


def build_measure(size):
    unit = '|....|'
    measure = [unit[1:] if m != 0 else unit for m in range(size - 1)]
    description = [str(n + 1).rjust(5) if n != 0 else str(n + 1) for n in range(size)]

    return ''.join(measure) + '\n' + ''.join(description)

print build_measure(14)

# 3.6


def build_rectangle():
    string = ''
    horizontal = '+---+---+---+---+'
    vertical = '|   |   |   |   |'

    string += 7 * (horizontal + '\n' + vertical + '\n') + horizontal

    return string

print build_rectangle()

# 3.8
S1 = [3, 4, 2, 7, 5, 6, 0]
S2 = [4, 12, 9, 4, 0, 16, 23, 2, 0]

print list(set(S1).intersection(S2))
print list(set(S1 + S2))

# 3.9
L = [[], [4], (1, 2), [3, 4], (5, 6, 7)]
print [sum(l) if isinstance(l, (list, tuple)) else l for l in L]

# 3.10

values = {
    'M': 1000,
    'D': 500,
    'C': 100,
    'L': 50,
    'X': 10,
    'V': 5,
    'I': 1
}

values1 = dict(
    M=1000,
    D=500,
    C=100,
    L=50,
    X=10,
    V=5,
    I=1
)

values2 = {}
values2['M'] = 1000
values2['D'] = 500
values2['C'] = 100
values2['L'] = 50
values2['X'] = 10
values2['V'] = 5
values2['I'] = 1
