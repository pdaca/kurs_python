# encoding: utf-8
import math


class Point:
    """Klasa reprezentująca punkty na płaszczyźnie."""

    def __init__(self, x=0, y=0):  # konstuktor
        self.x = x
        self.y = y

    def __str__(self):
        # zwraca string "(x, y)"
        return str((self.x, self.y))

    def __repr__(self):
        # zwraca string "Point(x, y)"
        return "Point({x}, {y})".format(x=self.x, y=self.y)

    def __eq__(self, other):
        # obsługa point1 == point2
        return (self.x, self.y) == (other.x, other.y)

    def __ne__(self, other):
        # obsługa point1 != point2
        return not (self.x, self.y) == (other.x, other.y)

    # Punkty jako wektory 2D.
    def __add__(self, other):
        return self.x + other.x, self.y + other.y

    def __sub__(self, other):
        return self.x - other.x, self.y - other.y

    def __mul__(self, other):
        return self.x * other.x + self.y * other.y

    def cross(self, other):
        return self.x * other.y - self.y * other.x

    def length(self):
        return math.sqrt(pow(self.x, 2) + pow(self.y, 2))
