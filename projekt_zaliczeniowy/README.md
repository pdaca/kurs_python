**Gra w statki** 


Każdy z graczy posiada po dwie plansze o wielkości od 5x5 do 10x10. 
Na jednym z kwadratów gracz zaznacza swoje statki, których położenie będzie odgadywał przeciwnik.
Nieodkryte pole oznaczone jest symbolem '-', niecelny strzał 'o', natomiast okręt to '+'. 
Wielkość okrętów oraz ich położenie jest losowane.

Gra jest możliwa tylko z AI. Program wykorzystuje algorytm korzystający ze 
stosu, który jeśli znajdzie cel, stara się zniszczyć statek.

Uruchomienie gry:


`python ./game.py`