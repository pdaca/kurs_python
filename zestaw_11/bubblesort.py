import timeit

iloscPowtorzen = 100


def mean(L):
    return sum(L) / len(L)


print "bubblesort 128 fullRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(128); d.fullRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 128 closeRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(128); d.closeRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 128 closeDivertedRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(128); d.closeDivertedRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 256 fullRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(256); d.fullRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 256 closeRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(256); d.closeRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 256 closeDivertedRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(256); d.closeDivertedRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 512 fullRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(512); d.fullRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 512 closeRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(512); d.closeRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 512 closeDivertedRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(512); d.closeDivertedRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 1024 fullRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(1024); d.fullRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 1024 closeRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(1024); d.closeRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"

print "bubblesort 1024 closeDivertedRandom:"
t = timeit.Timer("d.bubblesort()", "import sort; d = sort.Data(1024); d.closeDivertedRandom()")
w = t.repeat(iloscPowtorzen, 1)
print "min =", min(w)
print "srednia =", mean(w)
print "max =", max(w), "\n"
